# **Sass**

## **Desventajas de css plano**

- los estilos deben escribirse manualmente (no se pueden automatizar)
  - ejm : curso se necesita recorrer cierto tipos de datos para asignar estilos a un componente con css plano no se podria, (ayuda js)
- las guias de estilo del proyecto no pueden modificarse fácilmente
- No se puede modularizar el código sin afectar el rendimiento del proyecto
- Propenso a sobreescrituras, problemas de especificidad y cascada en proyectos grades y en equipos de trabajo

## **¿Qué es un preprocesador CSS?**

- Es un lenguaje que permite generar css a través de una sintaxis similar pero aprovechando caracteristicas de la programación (imports,funciones)
- sass - stylus - less
- PostCss no es un preprocesador (lee el css y hace las transformaciones respectivas)

### **Ventajas de un preprocesador**

    - Estructurar los estilos en módulos pero transpilarlos a un único archivo css resultante
    - Programar estilos automáticamente
    - Almacenar las guias de estilos del proyecto en variables
    - Hacer que proyectos grandes sean mantenibles entre equipos de trabajo 

- [sassmeister](https://www.sassmeister.com/)
- [sass](https://sass-lang.com/documentation/)
- [directivas](https://uniwebsidad.com/libros/sass/capitulo-7)
- [Sass](https://uniwebsidad.com/libros/sass)
- [variables css I](https://uniwebsidad.com/tutoriales/como-usar-las-nuevas-variables-css)
- [variables css II](https://scrimba.com/g/gcssvariables)
- [variables css III](https://www.freecodecamp.org/news/everything-you-need-to-know-about-css-variables-c74d922ea855/)
- [variables css IV](https://www.youtube.com/watch?v=PHO6TBq_auI&list=PL4-IK0AVhVjOT2KBB5TSbD77OmfHvtqUi&index=1)

**Opciones de compilacion :**

```sh
    node-sass -w scss -o css
    node-sass scss -o css
    node-sass scss -o css --out-style = expanded | compressed | nested | compact --source-comments
```

**Source Maps :**

Mapas de origen : nos proporcionan un vinculo entre el codigo resultante al codigo origen

```sh
    node-sass scss -o css --source-map=true
```

Verificar esto en el navegador , ya se puede ver el codigo scss
